---
title: How-to: Mitwirken via Git
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - git
        - mitwirken
page-toc:
    active: false
---


![](de/git.png)

# Was ist git?

**git** ist eine freie Software zur verteilten Versionsverwaltung, ein Werkzeug, um Dateien, Code und Inhalt zu verfolgen. Es ermöglicht vielen Menschen gleichzeitig, an den Codes zu arbeiten und allen Änderungen zu folgen, indem auf den Computern der Entwickler eine Kopie des Projekts vorliegt. git ist sehr beliebt bei Entwicklern und Systemadministratoren. Darüber hinaus können seine Funktionen überall dort Anwendung finden, wo der Verlauf von Änderungen und die Möglichkeit, Inhalt einzureichen und in Gruppen zusammenzuarbeiten, nötig sind.

Wir nutzen git als Hauptwerkzeug für die Entwicklung unserer How-tos und Website. Es ist auch das Werkzeug, das wir bevorzugen, hauptsächlich, weil seine Anwendung wirklich einfach und schnell und es gleichzeitig sehr mächtig ist. Für die Bearbeitung der Texte nutzen wir **Atom**, einen mächtigen Text- und Code-Editor, wobei Du natürlich einen beliebigen Texteditor Deiner Wahl benutzen kannst.

Auf den nächsten Seiten werden wir Dir zeigen, wie Du diese für die Arbeit an der Dokumentation von **Disroot** nutzen kannst.

### [git: Basics How-to](how-to-use-git)

----
Mehr Informationen über git findest Du [hier](https://de.wikipedia.org/wiki/Git) und in [diesem Onlinebuch](https://git-scm.com/book/de/v2).
