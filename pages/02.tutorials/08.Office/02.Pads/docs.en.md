---
title: 'Pads'
updated:
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - Pad
        - Etherpad
        - EtherCalc
page-toc:
  active: false
---

![](/home/icons/etherpad.png)
**Etherpad** and **EtherCalc** are collaborative applications for multi-user real-time document edition. You can reach them directly from your browser at: [https://pad.disroot.org](https://pad.disroot.org) and [https://calc.disroot.org](https://calc.disroot.org).<br>No user account is needed to make use of them. However our cloud comes with very handy plugin that helps you keep track of all your pads, just as if they are one of your files.|

!! ![](/home/icons/note.png)
!! "Pads" and "calcs" aren't files containing your data but are links to your work stored  on either https://pad.disroot.org or https://calc.disroot.org.|

# The idea behind pads...
... is very simple. It's a text/spreadsheet editor that lives in the web. Everything you type in gets written to your pad automatically. You can work on a document with multiple people at the same time without a need to save and pass copies of the documents to each other. Once your work is done, you can export the pad to a format of your choice.

### [Etherpad](etherpad)
- Collaborative web text documents editor

### EtherCalc (Coming Soon)
- Collaborative spreadsheet

### [Padland](padland)
- Pad management tool for Android
