---
title: Oficina
subtitle: "Blocks de notas, Pastebin, Encuestas & Compartir archivos"
icon: fa-file-text
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - oficina
        - encuestas
        - pastebin
        - archivos
        - compartir
        - disapp
page-toc:
    active: false
---

# Herramientas de Oficina

**Disroot** proporciona un conjunto de herramientas web que tal vez quieras probar.

---

Para sacarles mayor provecho, hay también una aplicación de **Disroot** que reúne todas estas herramientas y otros servicios:

### [DisApp](../user/disapp)
