---
title: Nube: Clientes para el móvil
taxonomy:
    category:
        - docs
visible: false
page-toc:
     active: false
---

Nextcloud se integra con tu dispositivo de manera muy sencilla, proporcionando una experiencia nativa para la mayoría de los dispositivos y sistemas operativos.
Aquí en Disroot intentamos incluir y documentarlos a todos para que sea sencillo para cualquiera ponerlos en marcha.

|Android|iOS|MacOS|
|:--:|:--:|:--:|
|[Aplicaciones de Nextcloud](android)|[Aplicación de iOS](ios)|[Aplicación de MacOS](mac-os)|
