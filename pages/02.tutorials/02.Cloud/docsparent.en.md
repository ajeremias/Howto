---
title: Cloud
subtitle: "Basics, settings, syncing, clients"
icon: fa-cloud
updated:
        last_modified: "April 2019"
        app: Nextcloud
        app_version: 15
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - cloud
page-toc:
    active: false
---

![](/home/icons/nc_logo.png)
**Nextcloud** is **Disroot**'s core service and the main user interface we try to integrate with most of the apps we offer. In this howto we would like to go through the basics of the interface, explaining the main UI (User interface) concept, as well as some basic actions on files and personal settings.
<br>

----

## What is Nextcloud?
**Nextcloud** is a free and open source software that allows you to upload and storage files on a server (it could be a server of your own), syncing them with different devices to which you can safely access from anywhere via internet. In addition, Nextcloud provides some interesting and useful features like calendars, contacts and bookmarks synchronization, call/video conference and news feeder.<br>

----------
