---
title: Cloud Apps: Bookmarks
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - bookmarks
visible: true
page-toc:
    active: false
---

## Bookmarks (coming soon)

### Web interface
- Creating, editing and exporting Bookmarks

### Desktop clients
- Desktop clients to manage your Bookmarks

### Mobile clients
- Bookmarks Apps, device settings
