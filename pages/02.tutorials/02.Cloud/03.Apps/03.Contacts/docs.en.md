---
title: Cloud Apps: Contacts
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - contacts
visible: true
page-toc:
    active: false
---

## Contacts

### [Web interface](web)
- Creating, editing and syncing contacts

### [Desktop clients](desktop)
- Desktop clients and integration settings for organizing and synchronizing your contacts

### [Mobile clients](/tutorials/cloud/clients/mobile)
- Mobile clients and settings for organizing and synchronizing your contacts
