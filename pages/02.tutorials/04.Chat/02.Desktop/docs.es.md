---
title: Chat: Clientes para el escritorio
published: true
visible: false
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

|**Pidgin**|**Gajim**|**Dino**|
|:--:|:--:|:--:|
|01.[GNU/Linux](pidgin/linux)<br>02.[Windows](pidgin/win)|01.[GNU/Linux](gajim/linux)<br>02.[Windows](gajim/win)|[GNU/Linux](/dino)|
