---
title: Email clients: Thunderbird
page-toc:
  active: true
visible: false
published: true
updated:
        last_modified: "April 2019"
        app: Thunderbird
        app_version: 60.6.1 for Manjaro Linux
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
  active: true
---

# Thunderbird

![](/home/icons/thunderbird.png)
**Thunderbird** is a powerful open-source email client, calendar and RSS-Feed manager.

## Install Thunderbird
If you don't have it installed yet, got to [https://www.mozilla.org/en-US/thunderbird/all/](https://www.mozilla.org/en-US/thunderbird/all/) and choose your language and Operating System.

!! ![](/home/icons/note.png)
!! NOTE: For **GNU/Linux** users we recommend using your distribution package manager to get the latest and updatable version of **Thunderbird**.

## **Open Thunderbird**
If it's the first time you run it, you will be directly greeted with account setup and you can skip directly to **step 3**.  
If you're using **Thunderbird** already and have some accounts setup, just select "**New Account**" icon in the root view.

![](en/thunderbird_setup2.png)

## **Fill in your account information**

![](en/thunderbird_setup3.png)

 - **Your name:** *Name that will be displayed in the* "From" *field*
 - **Email address:** *your_username @ disroot.org*
 - **Password:** *your_super_secret_password*
 - **Remember Password?:** If you want **Thunderbird** to remember your password and not prompt you for it every time you start the client, then select it.
 - Click "**Continue**" button once you're done and verified everything is correct.

**Thunderbird** now should auto-detect the needed settings like this:

![](en/thunderbird_setup4.png)

## **Done!** \o/

![](en/thunderbird_setup5.png)
