---
title: 'Email: Setup alias'
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
page-toc:
    active: false
---

# Email Aliases

Once you requested email aliases using this [form](https://disroot.org/en/forms/alias-request-form), you'll need to set them up. Below you can find how to do it on various email clients.

- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
- [FairEmail](fairemail)
- [Mail iOS](mailios)
