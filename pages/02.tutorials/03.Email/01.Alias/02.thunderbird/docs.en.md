---
title: Email Alias: Setup on Thunderbird
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
page-toc:
    active: true
---

# Setup Alias on Thunderbird

First of, start **Thunderbird** and go to account settings by right-clicking on your account.

![](en/identity_settings.gif)

On the bottom right of the settings window, you have **"Manage Identities..."** settings button.

Once in the Identity manager, you can add new alias by pressing **"Add..."** button and filling in the form:

![](en/identity_add.gif)

# Set default
If you want to set new email alias as your default one, just select the mail alias and click **"Set Default"** button.

![](en/identity_default.gif)

# Send email
To send email with your new alias, just click on the **"From"** field and select alias you want to use from the dropdown menu, when composing your mail.

![](en/identity_send.gif)
