---
title: Alias de correo: Configurar en K9
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
page-toc:
    active: true
---

## Configuración
Primero, inicia **K9** y ve a tus configuraciones de cuenta

![](es/identity_settings.png)

Cuando estés en Configuraciones, ve a la pestaña "**Envío de mensajes**" y teclea en "**Identidades**".

![](es/identity_settings2.png)

Selecciona "**Nueva identidad**" tecleando sobre el icono de los "tres puntos" arriba a la derecha.
*(Todo usuario de disroot tiene por defecto un alias usuario@disr.it que puede utilizar)*

![](es/identity_settings3.png)

Completa el formulario proporcionando el nuevo alias de dirección

![](es/identity_settings4.png)

## Establecer como predeterminado
Para cambiar la identidad predeterminada, en las configuraciones "**Administrar Identidades**", sólo teclea y presiona el alias que deseas establecer y selecciona la opción "**Mover al inicio / hacer predeterminada**".

![](es/identity_settings5.png)

## Enviando correo
Para enviar correo con tu nuevo alias, sólo haz click en el campo "**De**" y selecciona el alias que desees utilizar desde el menú desplegable, cuando estés redactando un correo.
