---
title: Hogyan igényelj alternatív domainneveket emailhez
published: true
visible: true
taxonomy:
    category:
        - docs

---

Az alternatív domainnevek a rendszeres támogatóinknak érhetőek el. Rendszeres támogatók alatt olyanokat értünk, akik havonta legalább egy csésze kávéra „hívnak meg” minket. Nem arról van szó, hogy a kávét reklámozzuk; a kávé igazából a [kihasználás és](http://thesourcefilm.com/) [egyenlőtlenség](http://www.foodispower.org/coffee/) találó jelképe. Azt gondoltuk, hogy ez egy jó módja annak, hogy le tudják mérni az emberek, mennyit tudnak adni.

Arra kérünk, fontold meg, hogy te is hozzájárulsz! Ha havonta „meg tudsz hívni” minket egy csésze Rio De Janeiró-i kávéra, rendben van, de ha tudsz fizetni nekünk havonta egy dupla koffeinmentes cappuccinót extra tejszínhabbal is, igazán segítesz a Disroot működtetésében és más, kevesebb pénzel rendelkezők számára is ingyenes marad.

Találtuk ezt a [listát](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) a kávé árairól világszerte. Lehet, hogy nem nagyon pontos, de ki lehet saccolni belőle, hogy mennyibe kerülnek.

Az alternatív domainnevek igényléséhez ki kell töltened ezt az [űrlapot](https://disroot.org/forms/alias-request-form).
