---
title: Administración de la Cuenta
published: true
indexed: true
visible: true
updated:
    last_modified: "Julio 2019"		
taxonomy:
    category:
        - docs
    tags:
        - usuario
        - administración
page-toc:
    active: false
---

# Administración de la Cuenta

#### [Administrar tu cuenta de Disroot](ussc/)

#### [Solicitar Alias de correo](alias-request/)
