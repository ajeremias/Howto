---
title: Amministrazione dell'account
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

### Guida:

#### [Gestisci il tuo account Disroot](ussc/)

#### [Richiedi un alias](alias-request)
