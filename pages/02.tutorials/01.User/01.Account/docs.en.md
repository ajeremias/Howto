---
title: Account Administration
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

### How-to:

#### [Manage your Disroot Account](ussc/)

#### [Request email Alias](alias-request)
