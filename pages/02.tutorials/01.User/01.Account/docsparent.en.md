---
title: Account Administration
published: true
indexed: true
visible: true
updated:
    last_modified: "July 2019"		
taxonomy:
    category:
        - docs
    tags:
        - user
        - administration
page-toc:
    active: false
---

# Account Administration

#### [Manage your Disroot Account](ussc/)

#### [Request email Alias](alias-request)
