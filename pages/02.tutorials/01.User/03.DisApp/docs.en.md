---
title: DisApp
published: true
visible: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: DisApp
    app_version: 1.2
taxonomy:
    category:
        - docs
    tags:
        - user
        - disapp
page-toc:
    active: false
---

# The Disroot's Community application
![](/home/icons/disapp.png)
**One App to rule them all**


Disrooter **Massimiliano** saw the potential of a **Disroot** app and decided to take the challenge with an unexpected approach. He developed the **Disroot "Swiss army knife"** app that helps and guides disrooters to recommended apps, tips and tutorials on how to set everything up. The app will pick the best (in our opinion) app for email, chat, etc, and for those services that don't have a dedicated app it will open them in the webview window. The app also provides directions to all community tutorials we've gathered over the years to help people use provided services.

The app is available from the one and only truly Free/Libre Android store: [here](https://f-droid.org/en/packages/org.disroot.disrootapp/).
