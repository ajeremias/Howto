---
title: Utente
subtitle: Account e gestione dei dati personali
icon: fa-user
published: true
visible: true
taxonomy:
    category:
        - docs
        - topic
page-toc:
    active: false
---

# Utente

In questa sezione puoi trovare alcune informazioni utili riguardanti la gestione del tuo account. 

<br>
