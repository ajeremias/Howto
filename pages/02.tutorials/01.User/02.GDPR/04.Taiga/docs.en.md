---
title: "Taiga: Project export"
published: true
visible: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Taiga
    app_version: 4.2
taxonomy:
    category:
        - docs
    tags:
        - user
        - taiga
        - gdpr
    visible: true
page-toc:
    active: false
---

Follow these steps to export your project's data from **Taiga**'s interface:

1. Go to https://board.disroot.org and log in.
2. select the project you want to export and go to the Admin's settings.

![](en/settings.png)

3. Under the **Project** tab you will find the **Export** option. Click the ![EXPORT](en/export_button.png) button to generate a .json file with all the project's information. You can use that as a backup or to start a new project based on it.

![](en/export.png)
