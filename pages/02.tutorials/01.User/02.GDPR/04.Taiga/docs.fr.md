---
title: "Taiga: Exporter vos Projets"
published: true
visible: true
indexed: true
updated:
    last_modified: "October 2019"		
    app: Taiga
    app_version: 4.2
taxonomy:
    category:
        - docs
    tags:
        - user
        - taiga
        - gdpr
    visible: true
page-toc:
    active: false
---

Sur Taiga, il n'est possible d'exporter les données des projets uniquement si vous en êtes l'administrateur. Si vous participez à un projet sans en être admin, vous devrez passez par un admin et leur demander d'exporter les données pour vous.

Suivez ces étapes pour exporter les données de vos projets sur **Taiga**:

1. Rendez vous sur https://board.disroot.org et connectez-vous.
2. Sélectionnez le projet que vous voulez exporter et aller dans les paramètres d'Administration.

![](en/settings.png)

3. Sous l'onglet **Projet** vous trouverez l'option **Export**. Cliquer sur le bouton ![EXPORT](en/export_button.png)  pour générer un fichier .json avec toutes les informations du projet. Vous pouvez l'utiliser comme backup ou pour démarrer un nouveau projet basé sur celui-ci.

![](en/export.png)
