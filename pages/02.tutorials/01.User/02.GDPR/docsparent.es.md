---
title: 'RGDP: Exportar Datos Personales'
visible: true
published: true
indexed: true
updated:
    last_modified: "Julio 2019"
taxonomy:
    category:
        - docs
    tags:
        - user
        - datos personales
        - rgdp
    visible: true
page-toc:
    active: false

---
# Cómo exportar tus Datos Personales

En **Disroot**, la mayoría de la información que guardamos es la que provees cuando utilizas los servicios (almacenamos tus archivos porque elegiste guardarlos en nuestra nube). No tenemos interés en obtener ni recopilar ningún dato adicional o procesar información para venderla a empresas de publicidad ni ningún otro fin de lucro. Por lo tanto, la mayoría de los servicios que proveemos te brindan alguna manera de auto exportar tus datos. Este capítulo contiene tutoriales que te ayudarán a obtener toda tu información almacenada por los servicios que provee **Disroot** y que están conectados a tu cuenta.

## Contenido
- [Nube (Nextcloud)](nextcloud)
  - [Archivos & Notas](nextcloud/files)
  - [Contactos](nextcloud/contacts)
  - [Calendarios](nextcloud/calendar)
  - [Noticias](nextcloud/news)
  - [Marcadores](nextcloud/bookmarks)
- [Diaspora*](diaspora)
- [Foro (Discourse)](discourse)
- [Tablero (Taiga)](taiga)
- [Hubzilla](hubzilla)
