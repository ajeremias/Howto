---
title: "Hubzilla: Esporta i contenuti del tuo canale"
published: true
visible: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Hubzilla
    app_version: 4.2
taxonomy:
    category:
        - docs
    tags:
        - user
        - hubzilla
        - gdpr
    visible: true
page-toc:
    active: false
---

Esistono alcuni modi per esportare i contenuti del tuo canale **Hubzilla**. Prima di tutto, devi accedere al tuo account **Hubzilla** su [https://hub.disroot.org] (https://hub.disroot.org) (non dimenticare di usare il tuo account di posta elettronica **Disroot** per tale accesso, (*username@disroot.org*).

# Esportare il canale
Vai a questo URL [https://hub.disroot.org/uexport/basic](https://hub.disroot.org/uexport/basic) per esportare le informazioni di base del tuo canale in un file. Questo funge da backup delle connessioni, autorizzazioni, profilo e dati di base, che possono essere utilizzati per importare i dati in un nuovo hub del server, ma non contengono i contenuti.

# Export Content
Vai a questo URL [https://hub.disroot.org/uexport/complete](https://hub.disroot.org/uexport/complete) 
per esportare le informazioni del canale e i contenuti recenti in un backup JSON che può essere ripristinato o importato in un altro hub server. Questo esegue il backup di tutte le connessioni, autorizzazioni, dati del profilo e diversi mesi di post. Questo file potrebbe essere MOLTO grande. Ti preghiamo di attendere: l'avvio del download potrebbe richiedere alcuni minuti.

Puoi anche esportare i tuoi post e le tue conversazioni per un determinato anno o mese. Regola la data nella barra degli indirizzi del browser per selezionare altre date. Se l'esportazione non riesce (probabilmente a causa dell'esaurimento della memoria nell'hub del server), riprovare selezionando un intervallo di date più limitato.

 - Per selezionare tutti i post di un determinato anno, ad esempio quest'anno, visitare: [https://hub.disroot.org/uexport/2019](https://hub.disroot.org/uexport/2019)

 - Per selezionare tutti i post per un determinato mese, ad esempio gennaio di quest'anno, visitare: [https://hub.disroot.org/uexport/2019/1](https://hub.disroot.org/uexport/2019/1)

Questi file di contenuto possono essere importati o ripristinati accedendo a "import_items" su qualsiasi sito contenente il tuo canale. Per risultati ottimali, importali o ripristinali in ordine di data (prima il più vecchio).
