---
title: "Hubzilla: Exporting your channel content"
published: true
visible: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Hubzilla
    app_version: 4.2
taxonomy:
    category:
        - docs
    tags:
        - user
        - hubzilla
        - gdpr
    visible: true
page-toc:
    active: false
---

There are few ways to export content of your **Hubzilla** channel. First of of course, you need to login to your **Hubzilla** account at [https://hub.disroot.org](https://hub.disroot.org) (don't forget to use your **Disroot** email account for that like, *username@disroot.org*)


# Export Channel
Go to this url [https://hub.disroot.org/uexport/basic](https://hub.disroot.org/uexport/basic) to export your basic channel information to a file. This acts as a backup of your connections, permissions, profile and basic data, which can be used to import your data to a new server hub, but does not contain your content.


# Export Content
Go to this url [https://hub.disroot.org/uexport/complete](https://hub.disroot.org/uexport/complete) to export your channel information and recent content to a JSON backup that can be restored or imported to another server hub. This backs up all of your connections, permissions, profile data and several months of posts. This file may be VERY large. Please be patient - it may take several minutes for this download to begin.

You may also export your posts and conversations for a particular year or month. Adjust the date in your browser location bar to select other dates. If the export fails (possibly due to memory exhaustion on your server hub), please try again selecting a more limited date range.

 - To select all posts for a given year, such as this year, visit [https://hub.disroot.org/uexport/2018](https://hub.disroot.org/uexport/2018)

 - To select all posts for a given month, such as January of this year, visit [https://hub.disroot.org/uexport/2018/1](https://hub.disroot.org/uexport/2018/1)

These content files may be imported or restored by visiting /import_items on any site containing your channel. For best results please import or restore these in date order (oldest first).
