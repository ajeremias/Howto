---
title: 'Nextcloud: Exporting Bookmarks'
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - bookmarks
        - gdpr
visible: true
page-toc:
    active: false
---

Exporting your bookmark data stored on the cloud is very easy with **Disroot**.

1. Login to the [cloud](https://cloud.disroot.org)

2. Select Bookmark app

![](en/select_app.gif)

3. Select Settings (on the bottom of the right sidebar) and press **"Export"** button

![](en/export.gif)
