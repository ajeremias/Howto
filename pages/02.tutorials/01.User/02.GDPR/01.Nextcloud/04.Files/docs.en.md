---
title: Nextcloud: Files & Notes
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - files
        - notes
        - gdpr
visible: true
page-toc:
    active: false
---

You can download your files as easy as in case of any **Nextcloud** app.

1. Login to [cloud](https://cloud.disroot.org)

2. Select **Files** app

3. Select all files by clicking on the checkbox

4. Then click on the **Actions** menu and select *Download*

![](en/files_app.gif)
