---
title: Nextcloud: Exporting Calendars
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
visible: true
page-toc:
    active: false
---

Exporting calendars is possible out of the box. Simply:

1. Login to the [cloud](https://cloud.disroot.org)

2. Select Calendar app

![](en/select_app.gif)

3. Export any on your calendars or calendars you are subscribed to.
Select *"three dot"* menu option next to calendar you want to export and hit *"Export"* option. Exported calendar is saved in .ics format.

![](en/export-calendar.gif)

Repeat the process for all other calendars you want to export.
