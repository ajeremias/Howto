---
title: "Nextcloud: Exporting Contacts"
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
visible: true
page-toc:
    active: false
---

Contacts can be exported at any time in a very simple way.

1. Login to your cloud account at [https://cloud.disroot.org](https://cloud.disroot.org)

2. Select "*Contacts*" App.

![](en/select_app.gif)

3. Hit the download button next to the addressbook you want to export. Contacts are saved in .vcf format.

![](en/export_data.gif)
