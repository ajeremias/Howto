---
title: GDPR: Nextcloud
published: true
indexed: false
taxonomy:
    category:
        - docs
    tags:
        - user
        - gdpr
visible: true
page-toc:
    active: false
---
## How-to export your...

  - [**Files and Notes**](files)
  - [**Contacts**](contacts)
  - [**Calendars**](calendar)
  - [**News**](news)
  - [**Bookmarks**](bookmarks)
