---
title: Nextcloud: Esportare le notizie (Feed RSS)
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - news
        - gdpr
visible: true
page-toc:
    active: false
---

Esportare i tuoi feed rss e le tue sottoscrizioni è molto semplice con l'applicazione **Nextcloud*.

1. Accedi al [cloud](https://cloud.disroot.org)

2. Seleziona l'applicazione **Notizie**

![](en/select_app.gif)

3. Seleziona *Impostazioni" in basso nella barra laterlae.

5. A dipendenza delle tue necessità puoi decidere di esportare:
   - Le tue sottoscrizioni (file .opml) - Questa opzione è particolarmente utile quando vuoi cambiare il tuo lettore di Feed RSS o semplicemente vuoi fare il backup delle tue sottoscrizioni.
   - Gli articoli non letti - Se vuoi esportare gli articoli che risultano non ancora letti per poterli leggere quando sei offline. 
   Attenzione: gli articoli marcati come letti verranno rimossi ad intervalli regolari.
   
![](en/export.gif)
