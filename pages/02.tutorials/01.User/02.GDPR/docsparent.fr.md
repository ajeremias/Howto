---
title: Exporter Données Personnelles / Conformité RGPD
visible: true
published: true
indexed: true
updated:
    last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - user
        - personal data
        - gdpr
    visible: true
page-toc:
    active: false

---
#  Comment exporter soi-même ses données personnelles

Chez **Disroot**, toutes les données collectées sont celles que vous fournissez en utilisant nos services (nous stockons vos fichiers parce que vous avez décidé de stocker vos fichiers sur notre cloud). Nous n'avons aucun intérêt dans l'acquisition et la collecte de données supplémentaires, ni dans le traitement de celles-ci en vue de les vendre à des compagnies publicitaires, ni en aucune façon de capitaliser dessus. Dans cette optique, la plupart de nos services vous offrent la possibilité d'exporter vos données vous-même. Ce chapitre inclut des tutoriels qui vous aideront à récupérer toutes les données stockées sur les serveurs de **Disroot** vous concernant/liées à votre compte.

## Table des matières
- [Cloud (Nextcloud)](nextcloud)
  - [Fichiers & Notes](nextcloud/files)
  - [Contacts](nextcloud/contacts)
  - [Calendriers](nextcloud/calendar)
  - [News](nextcloud/news)
  - [Favoris](nextcloud/bookmarks)
- [Diaspora*](diaspora)
- [Forum (Discourse)](discourse)
- [Boards (Taiga)](taiga)
- [Hubzilla](hubzilla)

---
