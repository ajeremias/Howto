---
title: Export Personal Data / GDPR Compliance
visible: true
published: true
indexed: true
updated:
    last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - user
        - personal data
        - gdpr
    visible: true
page-toc:
    active: false

---
#  How-to Self-export your personal data

At **Disroot** pretty much all data collected is the one you provide when using services (we store your files because you choose to store files on our cloud). We have no interest in acquiring and collecting any extra data nor processing it to sell to advertisement companies or in any way use to monetize on it. Therefore most of the services provides you with a way to self export your data. This chapter includes tutorials that will help you get all the data stored on services provided by **Disroot** and connected to your account.

## Contents
- [Cloud (Nextcloud)](nextcloud)
  - [Files & Notes](nextcloud/files)
  - [Contacts](nextcloud/contacts)
  - [Calendars](nextcloud/calendar)
  - [News](nextcloud/news)
  - [Bookmarks](nextcloud/bookmarks)
- [Diaspora*](diaspora)
- [Forum (Discourse)](discourse)
- [Board (Taiga)](taiga)
- [Hubzilla](hubzilla)

---
