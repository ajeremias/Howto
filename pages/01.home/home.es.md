---
title: Inicio
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Bienvenid@s al sitio de Guías y Tutoriales de Disroot.

El objetivo principal de este sitio es orientarte a través de los varios servicios de **Disroot**.

Cubrir todos los servicios, con todas las características provistas por **Disroot**, para todas las plataformas y/o Sistemas Operativos es un proyecto muy ambicioso, que demanda mucho tiempo y requiere mucho trabajo. Y como pensamos que puede ser beneficioso no solo para nuestrxs usuarixs (disrooters) sino para todas las comunidades de **Software Libre** y de **Código Abierto** que use los mismos programas o similares, toda ayuda de lxs disrooters es siempre necesaria y bienvenida.<br>
Así que, si piensas que falta un tutorial, que la información no es correcta o puede mejorarse, por favor, contáctanos o (mejor aún) empieza a escribir una guía o tutorial tú mismx.<br>

Para conocer las diferentes maneras en que puedes contribuir, por favor, revisa esta [sección](/contribute).
